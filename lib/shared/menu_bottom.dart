import 'package:flutter/material.dart';

class MenuBottom extends StatelessWidget {
  const MenuBottom({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
      onTap: (int index) {
        switch (index) {
          case 0:
            Navigator.pushNamed(context, '/page_index');
            break;
          case 1:
            Navigator.pushNamed(context, '/page_best');
            break;
          case 2:
            Navigator.pushNamed(context, '/page_recommendation');
          case 3:
            Navigator.pushNamed(context, '/page_my_book');
          default:
        }
      },
      items: const[
        BottomNavigationBarItem(icon: Icon(Icons.home), label: '홈'),
        BottomNavigationBarItem(icon: Icon(Icons.star), label: '인기'),
        BottomNavigationBarItem(icon: Icon(Icons.recommend), label: '추천'),
        BottomNavigationBarItem(icon: Icon(Icons.book), label: '내 서재'),
      ],
    );
  }
}
