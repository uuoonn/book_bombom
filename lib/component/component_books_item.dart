import 'package:book_bombom_app/model/books_item.dart';
import 'package:flutter/material.dart';

class ComponentBooksItem extends StatelessWidget {
  const ComponentBooksItem({
    super.key,
    required this.booksItem,
    required this.callback,

  });

  final BooksItem booksItem;
  final VoidCallback callback;


  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              width: 200,
              height: 300,
              child: Image.asset(
                booksItem.imgUrl,
                fit: BoxFit.fill,
              ),
            ),
            Container(
              width: 300,
              padding: EdgeInsets.all(10),
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.zero,
                    child: Text(
                      booksItem.booksTitle,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                      ),
                    ),
                  ),
                  Container(
                    child: Column(
                      children: [
                        Text(booksItem.booksInfo),
                        Text('정가 : ${booksItem.goodsPrice}원'),
                      ],
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
