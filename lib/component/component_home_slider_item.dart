import 'package:book_bombom_app/model/home_slider_item.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

class ComponentHomeSliderItem extends StatefulWidget {
  const ComponentHomeSliderItem({
    super.key,
    required this.homeSliderItem,
    required this.callback,

  });
  final HomeSliderItem homeSliderItem;
  final VoidCallback callback;

  @override
  State<ComponentHomeSliderItem> createState() => _ComponentHomeSliderItemState();
}
class _ComponentHomeSliderItemState extends State<ComponentHomeSliderItem> {

  late final HomeSliderItem homeSliderItem;
  late final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
        child: Column(
          children: [
            SizedBox(
              child: Image.asset(
                homeSliderItem.imgUrl,
                width: 200,
              ),
            ),
            Text(homeSliderItem.booksTitle),
            Text(homeSliderItem.booksInfo),
          ],
        ),
    );
  }
}
