import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

class PageHome extends StatefulWidget {
  const PageHome({super.key});

  @override
  State<PageHome> createState() => PageHomeState();
}

class PageHomeState extends State<PageHome> {

  final List<String> _list = [
    'assets/people.jpg',
    'assets/say.jpg',
    'assets/touch.jpg',
    'assets/wish.jpg',

  ];

  final List<String> _listName = [
    '사람을 어쩌고',
    '세이노',
    '닿을 수 있는 세상',
    '하는일마다'
  ];

  final CarouselController carouselController = CarouselController();
  int currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          CarouselSlider(
            carouselController: carouselController,
            options: CarouselOptions(
              autoPlay: true,
              autoPlayInterval: const Duration(seconds: 3),
              onPageChanged: ((index, reason) {
                setState(() {
                  currentIndex = index;
                });
              }),
            ),
            items: _list.map((String item) {
              return Image.asset(item, fit: BoxFit.cover);
            }).toList(),
          ),
          CarouselSlider(
            carouselController: carouselController,
            options: CarouselOptions(
              autoPlay: true,
              autoPlayInterval: const Duration(seconds: 3),
              onPageChanged: ((index, reason) {
                setState(() {
                  currentIndex = index;
                });
              }),
            ),
            items: _listName.map((String item) {
              return Text(item);
            }).toList(),
          ),
        ],
      ),
    );
  }
}
