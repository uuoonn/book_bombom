import 'package:book_bombom_app/model/home_slider_item.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

class PageHome1 extends StatefulWidget {
  const PageHome1({super.key});

  @override
  State<PageHome1> createState() => PageHome1State();
}

class PageHome1State extends State<PageHome1> {

  List<HomeSliderItem> _list = [
    HomeSliderItem(1, 'assets/people.jpg', '지긋지긋한 사람들을 죽이지않고 없애는 법', '현명한 인간관계란 뭘까?'),
    HomeSliderItem(2, 'assets/touch.jpg', '닿을 수 있는 세상', '인기의 장편소설 작가 신작'),
    HomeSliderItem(3, 'assets/say.jpg', '세이노의 가르침', '70만부 기념 리미티드'),

  ];

  final CarouselController carouselController = CarouselController();
  int currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Column(
        children: [
          CarouselSlider(
            carouselController: carouselController,
            options: CarouselOptions(
              autoPlay: true,
              autoPlayInterval: const Duration(seconds: 3),
              onPageChanged: ((index, reason) {
                setState(() {
                  currentIndex = index;
                });
              }),
            ), items: const <Widget>[

          ]
          ),
        ],
      ),
    );
  }
}
