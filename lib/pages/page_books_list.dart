import 'package:book_bombom_app/component/component_books_item.dart';
import 'package:book_bombom_app/model/books_item.dart';
import 'package:book_bombom_app/pages/page_books_detail.dart';
import 'package:flutter/material.dart';

class PageBooksList extends StatefulWidget {
  const PageBooksList({super.key});

  @override
  State<PageBooksList> createState() => _PageBooksListState();
}

class _PageBooksListState extends State<PageBooksList> {
  List<BooksItem> _list = [
    BooksItem(1, 'assets/people.jpg', '지긋지긋한 사람들', '현명한 인간관계란 뭘까?', 17000),
    BooksItem(2, 'assets/say.jpg', '세이노의 가르침', 'booksInfo', 18000),
    BooksItem(3, 'assets/touch.jpg', '닿을 수 있는 세상', 'booksInfo', 18000),
    BooksItem(4, 'assets/wish.jpg', '하는 일sssssss마다 잘 되리라', '긍정적인 사고방식 키우기', 17000),
    BooksItem(5, 'assets/say.jpg', '세이노의 가르침', 'booksInfo', 18000),
    BooksItem(6, 'assets/touch.jpg', '닿을 수 있는 세상', 'booksInfo', 18000),
  ];

  //_ 이 안에서만 쓴다
  //상대경로

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //resizeToAvoidBottomInset: false, -> body를 Single SingleChildScrollView로 감싸준다.
        appBar: AppBar(
          backgroundColor: Colors.grey[300],
          centerTitle: true,
          title: const Text(
            '인기 도서',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        body: Container(
          padding: EdgeInsets.all(20),
          child: GridView.builder(
            shrinkWrap: true,
            itemCount: _list.length,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 3, // 1개의 행에 보여줄 item 갯수
              childAspectRatio: 1/1.35, // item의 가로1, 세로1의 비율
              mainAxisSpacing: 10, // 수평 padding
              //crossAxisSpacing:10, // 수직 padding
            ),
            itemBuilder: (BuildContext ctx, int idx) {
              //buildContext : 순서대로 화면을 그려가는데(build), 그리는 과정에 필요한 모든 것들을 의미한다.
              //요구하는 이유는 화면 설계도를 수정해서 넣어야하기 때문이다.
              return ComponentBooksItem(
                  booksItem: _list[idx],
                  callback: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) =>
                            PageBooksDetail(booksItem: _list[idx])));
                  });
            },
          ),
        ));
  }
}
