import 'package:book_bombom_app/pages/page_best.dart';
import 'package:book_bombom_app/pages/page_books_list.dart';
import 'package:book_bombom_app/pages/page_home.dart';
import 'package:book_bombom_app/pages/page_my_book.dart';
import 'package:book_bombom_app/pages/page_recommend.dart';
import 'package:flutter/material.dart';

class PageApp extends StatefulWidget {
  const PageApp({super.key});

  @override
  State<PageApp> createState() => MyAppState();
}

class MyAppState extends State<PageApp> {

  int _selectedIndex = 0;

  final List<Widget> _navIndex = [
    PageHome(),
    PageBooksList(),
    PageRecommend(),
    PageMyBook(),
  ];

  void _onNavTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      // 앱바 메뉴아이콘, 타이틀, 검색아이콘

      appBar: AppBar(
        title: Text(
          'BOMBOM',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 25,
          ),
        ),
        centerTitle: true,
        //backgroundColor: Colors.pink,
        leading: IconButton(
          icon: Icon(Icons.menu),
          onPressed: () {},
        ),
        actions: [
          IconButton(
              onPressed: (){},
              icon: Icon(Icons.search))
        ],
      ),


      body: _navIndex.elementAt(_selectedIndex),
      bottomNavigationBar: BottomNavigationBar(
        fixedColor: Colors.pinkAccent,
        unselectedItemColor: Colors.grey,
        showUnselectedLabels: true,
        type: BottomNavigationBarType.fixed,
        items: const [
          BottomNavigationBarItem(
            icon: Icon(Icons.house),
            label: '홈',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.star),
            label: '인기',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.fmd_good),
            label: '추천',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.book),
            label: '내 서재',
          ),
        ],
        currentIndex: _selectedIndex,
        onTap: _onNavTapped,
      ),
    );
  }
}