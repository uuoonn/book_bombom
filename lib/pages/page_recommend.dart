import 'package:book_bombom_app/shared/menu_bottom.dart';
import 'package:flutter/material.dart';

class PageRecommend extends StatefulWidget {
  const PageRecommend({super.key});

  @override
  State<PageRecommend> createState() => PageRecommendState();
}

class PageRecommendState extends State<PageRecommend> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text(
          '추천페이지 입니당.',
        ),
      ),
    );
  }
}

