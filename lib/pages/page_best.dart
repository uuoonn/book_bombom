
import 'package:flutter/material.dart';

// 커뮤니티 페이지
class PageBest extends StatefulWidget {
  const PageBest({super.key});

  @override
  State<PageBest> createState() => PageBestState();
}

class PageBestState extends State<PageBest> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text(
          '인기글 페이지입니다.',
        ),
      ),
    );
  }
}

