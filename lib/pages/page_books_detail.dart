import 'package:book_bombom_app/model/books_item.dart';
import 'package:flutter/material.dart';

class PageBooksDetail extends StatefulWidget {
  const PageBooksDetail({
    super.key,
    required this.booksItem,
  });

  final BooksItem booksItem;

  @override
  State<PageBooksDetail> createState() => _PageBooksDetailState();
}

class _PageBooksDetailState extends State<PageBooksDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('상품 상세'),
      ),
      body: SingleChildScrollView(
        child: Container(
          alignment: Alignment.center,
          margin: EdgeInsets.all(40),
          child: Column(
            children: [
              Column(
                children: [
                  Image.asset(widget.booksItem.imgUrl),
                  Text('${widget.booksItem.booksTitle}'),
                  Text('${widget.booksItem.booksInfo}'),
                  Text('${widget.booksItem.goodsPrice}원')

                ],
              ),
            ],
          ),
        )
      ),
    );
  }
}
