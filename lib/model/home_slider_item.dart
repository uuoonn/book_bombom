
class HomeSliderItem {
  num id;
  String imgUrl;
  String booksTitle;
  String booksInfo;

  HomeSliderItem(this.id, this.imgUrl, this.booksTitle, this.booksInfo);
}